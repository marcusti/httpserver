BINDIR=./bin
BINARY=httpserver
TIMESTAMP=$(shell date --utc +%Y%m%d%H%M%S)

install:
	mkdir -p ${BINDIR}

	go get -u github.com/golang/dep/cmd/dep
	go get -u github.com/golang/lint/golint
	go get -u golang.org/x/tools/cmd/goimports
	go get -u github.com/kisielk/errcheck
	dep ensure

test:
	go test -v $(shell go list ./... | grep -v /vendor/)

test-race:
	go test -race -v $(shell go list ./... | grep -v /vendor/)

build: test
	rm -f ${BINDIR}/${BINARY}
	go build -o ${BINDIR}/${BINARY} gitlab.com/marcusti/httpserver/examples

build-all: test-race
	rm -f ${BINDIR}/${BINARY}
	dep ensure 
	golint $(shell go list ./... | grep -v /vendor/)
	go vet $(shell go list ./... | grep -v /vendor/)
	errcheck $(shell go list ./... | grep -v /vendor/)
	go build -race -o ${BINDIR}/${BINARY} gitlab.com/marcusti/httpserver/examples

run: build
	${BINDIR}/${BINARY}
