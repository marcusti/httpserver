package bind

import (
	"encoding/json"
	"net/http"
)

// Validator interface
type Validator interface {
	Validate() error
}

// JSON decodes the request body into the provided interface
func JSON(r *http.Request, v Validator) error {
	dec := json.NewDecoder(r.Body)
	defer func() {
		if err := r.Body.Close(); err != nil {
			panic(err)
		}
	}()
	if err := dec.Decode(v); err != nil {
		return err
	}
	if err := v.Validate(); err != nil {
		return err
	}
	return nil
}
