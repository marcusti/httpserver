package handler

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/marcusti/httpserver/bind"
	"gitlab.com/marcusti/httpserver/render"
	"gitlab.com/marcusti/httpserver/server"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type handler struct {
	render.Render
}

// New constructs a new http handler
func New(logger *log.Logger) http.Handler {
	h := handler{Render: render.Render{IndentJSON: "  "}}

	r := chi.NewRouter()
	r.Use(middleware.Recoverer)

	r.Route("/api", func(r chi.Router) {
		r.Post("/post", h.postExample)
		r.Get("/info", h.info)
		r.Get("/ping", h.ping)
	})
	r.Get("/static/*", server.FileServer(logger, "examples/static", "/static"))

	return r
}

type exampleRequest struct {
	Msg string `json:"msg"`
}

func (r *exampleRequest) Validate() error {
	if r.Msg == "" {
		return fmt.Errorf("msg may not be empty")
	}
	return nil
}

func (h *handler) postExample(w http.ResponseWriter, r *http.Request) {
	data := new(exampleRequest)
	if err := bind.JSON(r, data); err != nil {
		h.JSON(w, http.StatusBadRequest, "invalid post data: "+err.Error())
		return
	}
	h.JSON(w, http.StatusOK, data)
}

func (h *handler) ping(w http.ResponseWriter, r *http.Request) {
	h.Text(w, http.StatusOK, "OK")
}
