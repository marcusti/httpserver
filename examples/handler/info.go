package handler

import "net/http"

type infoResponse struct {
	Version string `json:"version"`
}

func (h *handler) info(w http.ResponseWriter, r *http.Request) {
	h.JSON(w, http.StatusOK, infoResponse{
		Version: "0.1",
	})
}
