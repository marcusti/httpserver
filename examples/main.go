package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/marcusti/httpserver/examples/handler"
	"gitlab.com/marcusti/httpserver/server"
)

func main() {
	host := flag.String("h", "localhost", "http server host")
	port := flag.Int("p", 3000, "http server port")
	flag.Parse()

	flags := log.LstdFlags | log.Lmicroseconds | log.Lshortfile
	logger := log.New(os.Stdout, "", flags)
	handler := handler.New(logger)

	srv := server.New(*host, *port, logger, handler)
	if err := srv.Start(); err != nil {
		srv.Logger.Fatalln(err)
	}
}
