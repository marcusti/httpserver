package render

import (
	"bytes"
	"encoding/json"
	"net/http"
)

// Render options
type Render struct {
	IndentJSON string
}

// JSON renders data as JSON
func (r *Render) JSON(w http.ResponseWriter, status int, v interface{}) {
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	enc.SetEscapeHTML(true)
	if r.IndentJSON != "" {
		enc.SetIndent("", r.IndentJSON)
	}
	if err := enc.Encode(v); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	_, err := w.Write(buf.Bytes())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// HTML renders a string as html
func (r *Render) HTML(w http.ResponseWriter, status int, v string) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(status)
	_, err := w.Write([]byte(v))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Text renders a string as plain text
func (r *Render) Text(w http.ResponseWriter, status int, v string) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(status)
	_, err := w.Write([]byte(v))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
