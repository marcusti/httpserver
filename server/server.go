package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"time"
)

// Server defines parameters for running an HTTP server.
type Server struct {
	http.Server
	Logger *log.Logger
}

// New constructs a new HTTP server.
func New(host string, port int, logger *log.Logger, handler http.Handler) Server {
	return Server{
		Logger: logger,
		Server: http.Server{
			Addr:    fmt.Sprintf("%s:%d", host, port),
			Handler: handler,
		},
	}
}

// Start starts the HTTP server.
// Gracefully shuts the server down when an interrupt signal is received.
func (srv *Server) Start() error {
	start := time.Now()

	errors := make(chan error, 1)
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	go func() {
		srv.Logger.Printf("starting http server at http://%s", srv.Addr)
		if err := srv.ListenAndServe(); err != nil {
			errors <- err
		}
		close(errors)
	}()

	select {
	case err := <-errors:
		return err
	case <-signals:
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		if err := srv.Shutdown(ctx); err != nil {
			return fmt.Errorf("error shutting down http server: %v", err)
		}
	}

	srv.Logger.Printf("server shut down after %v. bye.", time.Since(start))
	return nil
}

// FileServer creates a new file server relative to the current directory
func FileServer(logger *log.Logger, dir, prefix string) http.HandlerFunc {
	wd, err := os.Getwd()
	if err != nil {
		logger.Fatalf("could not get current directory: %v", err)
	}
	path := filepath.Join(wd, dir)
	fs := http.StripPrefix(prefix, http.FileServer(http.Dir(path)))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	})
}
