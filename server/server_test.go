package server_test

import (
	"bytes"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"
	"time"

	"gitlab.com/marcusti/httpserver/examples/handler"
	"gitlab.com/marcusti/httpserver/server"
)

func TestServer(t *testing.T) {
	tt := []struct {
		name      string
		path      string
		code      int
		checkBody bool
		body      string
	}{
		{"blank", "", 404, true, "404 page not found"},
		{"root", "/", 404, true, "404 page not found"},
		{"info", "/api/info", 200, false, ""},
		{"ping", "/api/ping", 200, true, "OK"},
		{"static", "/static/app.js", 200, false, ""},
	}

	Chdir(t, "..")
	logger := log.New(os.Stdout, "", log.Lshortfile)
	handler := handler.New(logger)
	srv := server.New("localhost", 9999, logger, handler)

	go func() {
		if err := srv.Start(); err != nil {
			t.Fatal(err)
		}
	}()
	time.Sleep(100 * time.Millisecond)

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			r, err := http.Get("http://localhost:9999" + tc.path)
			if err != nil {
				t.Fatalf("GET request failed: %v", err)
			}
			if r.StatusCode != tc.code {
				t.Fatalf("expected status code %d, got %d", tc.code, r.StatusCode)
			}
			if tc.checkBody {
				defer Close(t, r.Body)
				b, err := ioutil.ReadAll(r.Body)
				if err != nil {
					t.Fatalf("error reaging response body: %v", err)
				}
				body := string(bytes.TrimSpace(b))
				if body != tc.body {
					t.Fatalf("expected body '%s', got '%s'", tc.body, body)
				}
			}
		})
	}

	go func() {
		// start a 2nd instance on the same port and expect an error
		err := srv.Start()
		if err == nil || err.Error() != "listen tcp 127.0.0.1:9999: bind: address already in use" {
			t.Errorf("expected error 'address already in use', but got: %v", err)
		}
	}()
	time.Sleep(100 * time.Millisecond)
}

func Chdir(t *testing.T, dir string) {
	if err := os.Chdir(dir); err != nil {
		t.Fatal(err)
	}
}

func Close(t *testing.T, c io.Closer) {
	if err := c.Close(); err != nil {
		t.Fatal(err)
	}
}
